import struct

def decode_notification(data):
    name, size, hash_value, creation_date = struct.unpack("<8s I I Q", data)
    name = name.decode("utf-8")
    size = size 
    hash_value = hash_value 
    creation_date_str = str(creation_date)

    return {
        "name": name,
        "size": size,
        "hash": hash_value,
        "creation_date": creation_date_str
    }

bytearrays = [
    bytearray(b'T10\x00\x00\x00\x00\x00_\x08\x00\x00n\xe1\x0b\xfe\xd8v$\x87\xdc\x00\x00\x00'),
    bytearray(b'T11\x00\x00\x00\x00\x00\xcc\x07\x00\x00\xc9Y\x01\xf0\xf8\xc4$\x87\xdc\x00\x00\x00'),
    bytearray(b'T13\x00\x00\x00\x00\x00\xfd\x07\x00\x00\xf2\xd0Z90\x0f%\x87\xdc\x00\x00\x00'),
    bytearray(b'T5790\x00\x00\x00\n\x03\x00\x00\xb7\x81\xbceP]%\x87\xdc\x00\x00\x00'),
    bytearray(b'T1\x00\x00\x00\x00\x00\x00\xd4\xa9\x01\x00bI\xe1\xfb\x90\x15M\x9c\x8d\x01\x00\x00')
]
decoded_data = decode_notification(bytearrays)
print(decoded_data)