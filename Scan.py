import asyncio
from bleak import BleakScanner
from bleak import BleakClient

async def main():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)

asyncio.run(main())


address = "D2:E2:10:BF:17:B0"
MODEL_NBR_UUID = "1400"

async def main(address):
    async with BleakClient(address) as client:
        model_number = await client.read_gatt_char(MODEL_NBR_UUID)
        print("Model Number: {0}".format("".join(map(chr, model_number))))

asyncio.run(main(address))
